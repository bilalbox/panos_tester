# panos_tester
> Tool for displaying matched security rule in PANOS firewall based on session details (e.g. source and destination zone, IP, port)) in a simple web interface.

[![MIT License](https://img.shields.io/badge/license-MIT-007EC7.svg)](LICENSE)

This program takes either a Firewall or Panorama IP and displays the security rule that would be applied to a session based on user-inputted test parameters. Output from selected operational mode commands can also be displayed via the "RUN CLI COMMANDS VIA API" page. See [db.json\["commands"\]](utils/db.json) for the current list of supported commands.


## Requirements
- xmltodict
- requests
- flask
- flask-wtf
- tinydb

## Usage
![web gui usage demo](panos_test_demo.gif?raw=true "web gui demo")

1. Run the associated [docker container](https://hub.docker.com/r/bilalbox/panos_tester):
docker run -i -t -p 5000:5000 --name=test bilalbox/panos_tester:latest

2. Access the web UI from the docker host by visiting https://{docker_host}:5000 in your favorite browser

3. Input your device details via the "CONFIGURE DEVICE" link (this actually modifies [db.json](utils/db.json) in the background).

4. If the device is a panorama, follow the "SELECT FIREWALL" link and select the connected firewall to test.

5. Input session test parameters on the "RUN *** POLICY TEST" page. 

6. View results


## Contributing

1. Fork it (<https://gitlab.com/bilalbox/panos_tester/forks/new>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
6. Make sure to add yourself to the list of [AUTHORS](AUTHORS)

## License

Distributed under the MIT license. See [``LICENSE``](LICENSE) for more information.