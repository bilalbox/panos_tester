from utils import fn
import requests
import logging
import tinydb


##############################################################
# MAIN FUNCTION
##############################################################
def main():

    # Suppress warnings from requests library
    requests.packages.urllib3.disable_warnings()

    # Enable logging
    logging.basicConfig(level=logging.INFO, filename='exceptions.log')

    # DB Stuff
    db = tinydb.TinyDB(
        'utils/db.json',
        sort_keys=True,
        indent=4,
        separators=(',', ': ')
        )

    # # BUILD DICTIONARY FOR "SHOW DEVICES CONNECTED" AND EMULATE API CALL
    db.table('credentials').purge()
    USERNAME = 'admin'
    PASSWORD = 'admin'
    HOST = '192.168.55.10'
    URL = f'https://{HOST}/api?'
    API_KEY = fn.keygen(USERNAME, PASSWORD, URL)
    db.table('credentials').insert({
        'username': USERNAME,
        'password': PASSWORD,
        'host': HOST,
        'url': URL,
        'api_key': API_KEY
        })
    s = db.table('credentials').get(doc_id=1)
    # devices = fn.get_devices(s['url'], s['api_key'])

    # BUILD DICTIONARY FROM TEST PARAMETERS
    test_dict = {
        'from': 'inside',
        'to': 'outside',
        'src': '172.16.0.11',
        'dst': '9.9.9.9',
        'dport': '53',
        'protocol': '17',
        'application': 'dns',
    }

    # TEST INDIVIDUAL FIREWALL
    URL = s['url']
    API_KEY = s['api_key']
    # print('\nRUNNING SECURITY POLICY TEST #1...')
    # result = fn.test_sec_policy(URL, API_KEY, test_dict)
    # for rule in result:
    #     for k, v in rule.items():
    #         print(f'{k} = {v}')

    print('\nRUNNING NAT POLICY TEST #1...')
    result = fn.test_nat_policy(URL, API_KEY, test_dict)
    for rule in result:
        for k, v in rule.items():
            print(f'{k} = {v}')

    # print('\nGETTING APPS...')
    # result = fn.get_apps(URL, API_KEY)
    # print(result)

    # commands = db.table('commands').get(doc_id=1)
    # print('\nGETTING ARP INFOS...')
    # result = fn.run_cmd(URL, API_KEY, commands['show arp all'])
    # print(result)

    # print('\nGETTING INTERFACES INFOS...')
    # result = fn.run_cmd(URL, API_KEY, commands['show interface logical'])
    # print(result)


##############################################################
# RUN IT!
##############################################################
if __name__ == '__main__':
    main()
